https://ghost.org/blog/templates/

# WORKING TITLE

intro

## SECTION 1

## SECTION 2

## SECTION 3

***

conclusion

<small>Image credits:</small>

---

# WORKING TITLE

intro

## SECTION 1

- what this section is about
- why it matters
- research or examples
- takeaways

## SECTION 2

- what this section is about
- why it matters
- research or examples
- takeaways

---

# WORKING TITLE

intro

## THE SETUP
- set the scene

## SECTION 1
- what's this section about?
- give an example or two
- why does it matter?

## SECTION 2
- what's this section about?
- give an example or two
- why does it matter?

## SECTION 3
- what's this section about?
- give an example or two
- why does it matter?

## SECTION 4
- what's this section about?
- give an example or two
- why does it matter?

## SECTION 5
- what's this section about?
- give an example or two
- why does it matter?

## SECTION 6
- what's this section about?
- give an example or two
- why does it matter?

## WHAT TO DO NEXT

1.
2.
3.

conclusion

<small>Image credits:</small>

---

# WORKING TITLE

summary text

intro

## SET UP
- what happened?
- why is it relevant?

## SECTION 1: back story

- what happened before
- why you decided to do something different
- takeaway

## SECTION 2: what changed, or what new thing did you try?

- what changed/what you did differently
- what you thought the outcome would be
- takeaway

## SECTION 3: what were the results?

- what was the outcome?
- any surprises?
- takeaway

## How to apply this to your business

---